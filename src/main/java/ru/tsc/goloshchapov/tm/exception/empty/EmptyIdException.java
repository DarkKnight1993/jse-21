package ru.tsc.goloshchapov.tm.exception.empty;

import ru.tsc.goloshchapov.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Exception! Id is empty!");
    }

    public EmptyIdException(final String value) {
        super("Exception! " + value + " id is empty!");
    }

}
