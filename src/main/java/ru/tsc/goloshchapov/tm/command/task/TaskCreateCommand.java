package ru.tsc.goloshchapov.tm.command.task;

import ru.tsc.goloshchapov.tm.command.AbstractTaskCommand;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return "task-create";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create a new task";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
    }

}
