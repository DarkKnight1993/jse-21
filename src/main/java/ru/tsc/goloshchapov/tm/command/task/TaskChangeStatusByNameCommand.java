package ru.tsc.goloshchapov.tm.command.task;

import ru.tsc.goloshchapov.tm.command.AbstractTaskCommand;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.exception.entity.TaskNotFoundException;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByNameCommand extends AbstractTaskCommand {
    @Override
    public String name() {
        return "task-change-status-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change task status by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE TASK STATUS BY NAME]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = serviceLocator.getTaskService().changeStatusByName(userId, name, status);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("[OK]");
    }

}
