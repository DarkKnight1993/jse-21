package ru.tsc.goloshchapov.tm.command.system;

import ru.tsc.goloshchapov.tm.command.AbstractSystemCommand;

public final class AboutShowCommand extends AbstractSystemCommand {
    @Override
    public String name() {
        return "about";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public String description() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Vladimir Goloshchapov");
        System.out.println("E-MAIL: goloschapov@tsconsulting.com");
    }

}
