package ru.tsc.goloshchapov.tm.component;

import ru.tsc.goloshchapov.tm.api.repository.*;
import ru.tsc.goloshchapov.tm.api.service.*;
import ru.tsc.goloshchapov.tm.command.AbstractCommand;
import ru.tsc.goloshchapov.tm.command.auth.*;
import ru.tsc.goloshchapov.tm.command.project.*;
import ru.tsc.goloshchapov.tm.command.projecttask.ProjectTaskBindByIdCommand;
import ru.tsc.goloshchapov.tm.command.projecttask.ProjectTaskShowByIdCommand;
import ru.tsc.goloshchapov.tm.command.projecttask.ProjectTaskUnbindByIdCommand;
import ru.tsc.goloshchapov.tm.command.system.*;
import ru.tsc.goloshchapov.tm.command.task.*;
import ru.tsc.goloshchapov.tm.constant.TerminalConst;
import ru.tsc.goloshchapov.tm.enumerated.Role;
import ru.tsc.goloshchapov.tm.enumerated.Status;
import ru.tsc.goloshchapov.tm.exception.entity.CommandNotFoundException;
import ru.tsc.goloshchapov.tm.model.Project;
import ru.tsc.goloshchapov.tm.model.Task;
import ru.tsc.goloshchapov.tm.repository.*;
import ru.tsc.goloshchapov.tm.service.*;
import ru.tsc.goloshchapov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final IAuthRepository authRepository = new AuthRepository();

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectService projectService = new ProjectService(projectTaskService, projectRepository);

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService, authRepository);

    private final ILogService logService = new LogService();

    private void initData() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
        projectService.add(userService.findByLogin("test").getId(), new Project("Project Gamma", "-", Status.COMPLETED));
        projectService.add(userService.findByLogin("admin").getId(), new Project("Project Alpha", "-"));
        projectService.add(userService.findByLogin("test").getId(), new Project("Project Beta", "-", Status.IN_PROGRESS));
        projectService.add(userService.findByLogin("test").getId(), new Project("Project Delta", "-", Status.COMPLETED));
        taskService.add(userService.findByLogin("test").getId(), new Task("Task Gamma", "-", Status.COMPLETED));
        taskService.add(userService.findByLogin("admin").getId(), new Task("Task Alpha", "-"));
        taskService.add(userService.findByLogin("test").getId(), new Task("Task Beta", "-", Status.IN_PROGRESS));
        taskService.add(userService.findByLogin("test").getId(), new Task("Task Delta", "-", Status.COMPLETED));
    }

    {
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectShowByNameCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskClearCommand());
        registry(new TaskCreateCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByNameCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new ProjectTaskBindByIdCommand());
        registry(new ProjectTaskUnbindByIdCommand());
        registry(new ProjectTaskShowByIdCommand());

        registry(new AboutShowCommand());
        registry(new ArgumentsListShowCommand());
        registry(new CommandsListShowCommand());
        registry(new ExitCommand());
        registry(new HelpShowCommand());
        registry(new InfoShowCommand());
        registry(new VersionShowCommand());

        registry(new AuthChangePasswordCommand());
        registry(new AuthLoginCommand());
        registry(new AuthLogoutCommand());
        registry(new AuthRegistryCommand());
        registry(new AuthUpdateProfileCommand());
        registry(new AuthViewProfileCommand());
    }

    private void registry(final AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void start(String[] args) {
        System.out.println("\n** WELCOME TO TASK MANAGER **");
        initData();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private void process() {
        logService.debug("Test environment");
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            try {
                System.out.println("\nENTER COMMAND:");
                command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                logService.info("Completed");
            } catch (final Exception exception) {
                logService.error(exception);
            }
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        AbstractCommand command = commandService.getCommandByArg(args[0]);
        if (command == null) throw new CommandNotFoundException(args[0]);
        command.execute();
        return true;
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotFoundException();
        abstractCommand.execute();
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }
}
