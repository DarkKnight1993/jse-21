package ru.tsc.goloshchapov.tm.repository;

import ru.tsc.goloshchapov.tm.api.repository.IUserRepository;
import ru.tsc.goloshchapov.tm.model.User;

import java.util.Optional;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return entities.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return entities.stream()
                .filter(e -> email.equals(e.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeUser(final User user) {
        entities.remove(user);
        return user;
    }

    @Override
    public User removeByLogin(final String login) {
        final Optional<User> user = Optional.ofNullable(findByLogin(login));
        user.ifPresent(this::removeUser);
        return user.orElse(null);
    }

}
